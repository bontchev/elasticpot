# Testing the Honeypot

Once the honeypot is set up and running at a particular IP address, you can
use the program `test.py` to test that it is working.

The general usage of the program is:

```prompt
usage: test.py [-h] [-v] [-P PORT] [-H HOST] [-o OUTPUTFILE]
               urlfile [urlfile ...]

Test the ElasticPot honeypot

positional arguments:
  urlfile               File with URLs to test

options:
  -h, --help            show this help message and exit
  -v, --version         show program's version number and exit
  -P PORT, --port PORT  Port to send data to (default: 9200)
  -H HOST, --host HOST  Host to communicate with (default: 127.0.0.1)
  -o OUTPUTFILE, --output OUTPUTFILE
                        Output file (default: stdout)
```

To test the honeypot with a set of URLs that it can support, use the script
like this:

```bash
cd ~/siphoney/test
python ./test.py -o output testurls.txt
diff output baseline
```

The contents of the created file `output` should be identical to the existing
file `baseline`.
