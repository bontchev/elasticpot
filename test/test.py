#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

__description__ = 'Test the ElasticPot honeypot'
__license__ = 'GPL'
__VERSION__ = '1.0.0'
__author__ = 'Vesselin Bontchev'
__email__ = 'vbontchev@yahoo.com'


from argparse import ArgumentParser
from sys import stderr, stdout

try:
    from requests import get, codes
except ImportError:
    print('Could not import module "requests"; try "pip install requests".', file=stderr)
    exit(1)


def get_options():
    parser = ArgumentParser(description=__description__)

    parser.add_argument('-v', '--version', action='version', version='%(prog)s version ' + __VERSION__)
    parser.add_argument('-P', '--port', type=int, default=9200,
                        help='Port to send data to (default: %(default)d)')
    parser.add_argument('-H', '--host', default='127.0.0.1',
                        help='Host to communicate with (default: %(default)s)')
    parser.add_argument('-o', '--output', dest='outputfile', default=None,
                        help='Output file (default: stdout)')
    parser.add_argument('urlfile', nargs='+',
                        help='File with URLs to test')
    args = parser.parse_args()
    return args


def main():
    args = get_options()

    output_file = open(args.outputfile, 'w') if args.outputfile is not None else stdout

    for filename in args.urlfile:
        try:
            with open(filename, 'r') as f:
                for url in f:
                    request = 'http://{}:{}{}'.format(args.host, args.port, url.strip())
                    print('Testing "{}":'.format(url.strip()), file=output_file)
                    r = get(request)
                    if r.status_code == codes.ok:
                        print(r.text, file=output_file)
                    else:
                        print('HTTP Error {}.'.format(r.status_code), file=output_file)
                    print('', file=output_file)
        except FileNotFoundError:
            print('Error: File "{}" not found.'.format(filename), file=stderr)

    if args.outputfile is not None:
        output_file.close()


if __name__ == '__main__':
    main()
