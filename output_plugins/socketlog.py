
from json import dumps
from socket import socket, AF_INET, SOCK_STREAM

from core import output
from core.config import CONFIG


class Output(output.Output):
    """
    socketlog output
    """

    def start(self):
        timeout = CONFIG.getint('output_socketlog', 'timeout')
        addr = CONFIG.get('output_socketlog', 'address')
        host = addr.split(':')[0]
        port = int(addr.split(':')[1])

        self.sock = socket(AF_INET, SOCK_STREAM)
        self.sock.settimeout(timeout)
        self.sock.connect((host, port))

    def stop(self):
        self.sock.close()

    def write(self, event):
        message = dumps(event) + '\n'

        try:
            self.sock.sendall(message.encode())
        except OSError as ex:
            if ex.errno == 32:  # Broken pipe
                self.start()
                self.sock.sendall(message.encode())
            else:
                raise
