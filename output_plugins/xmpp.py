
from core import output
from core.config import CONFIG
from time import gmtime, strftime

from xmpp import Client
from xmpp.protocol import JID, Message


class Output(output.Output):
    """
    xmpp output
    """

    def start(self):
        jabberid = CONFIG.get('output_xmpp', 'user')
        password = CONFIG.get('output_xmpp', 'password')
        self.receiver = CONFIG.get('output_xmpp', 'muc')

        jid = JID(jabberid)
        self.connection = Client(server=jid.getDomain(), debug=None)
        self.connection.connect()
        self.connection.auth(user=jid.getNode(), password=password, resource=jid.getResource())

    def stop(self):
        pass

    def write(self, event):
        message = '[{} UTC] [ElasticPot on {}]: {} ({} {}) from {}:{}.'.format(
            strftime('%Y-%m-%d %H:%M:%S', gmtime(event['unixtime'])),
            event['sensor'], event['message'], event['request'],
            event['url'], event['src_ip'], event['dst_port']
        )

        self.connection.send(Message(to=self.receiver, body=message))
