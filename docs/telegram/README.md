# Sending the output of the honeypot to a Telegram channel

- [Sending the output of the honeypot to a Telegram channel](#sending-the-output-of-the-honeypot-to-a-telegram-channel)
  - [Prerequisites](#prerequisites)
  - [Create a Telegram bot and get its token](#create-a-telegram-bot-and-get-its-token)
  - [Get the chat ID of a channel](#get-the-chat-id-of-a-channel)
  - [Configure the honeypot](#configure-the-honeypot)

## Prerequisites

- Working honeypot installation
- The Telegram app installed on a mobile device
- A Telegram bot and its token
- A Telegram channel and its channel ID

## Create a Telegram bot and get its token

1. Install the Telegram app on a mobile device and register a phone number
there. It *has* to be a mobile phone number (a landline won't do) and you
*have* to have control of it. You must be able to receive voice calls and SMS
messages on it. Keep in mind that if you lose control of that phone number,
somebody else who gets control of it will be able to take over your Telegram
account. If, when registering your phone number, you get a message that an
internal error has occurred, wait a day or two and try again.
1. Open the Telegram application then search for `@BotFather`. Make sure that
it is labeled as "Bot", or might might be some malicious Telegram user trying
to masquerade as it.
1. Click Start
1. Click Menu and select /newbot or type `/newbot` and hit Send
1. Follow the instructions until you get message like this

```@BotFather
    Done! Congratulations on your new bot. You will find it at t.me/new_bot.
    You can now add a description.....

    Use this token to access the HTTP API:
    63xxxxxx71:AAFoxxxxn0hwA-2TVSxxxNf4c
    Keep your token secure and store it safely, it can be used by anyone to control your bot.

    For a description of the Bot API, see this page: https://core.telegram.org/bots/api
```

<!-- markdownlint-disable MD029 -->
6. Optionally, edit your bot's properties - permissions, screen name, etc.
1. Your bot token is `63xxxxxx71:AAFoxxxxn0hwA-2TVSxxxNf4c`. Make sure to keep it
confidential because anyone who has it can take full control of the bot.
<!-- markdownlint-enable MD029 -->

## Get the chat ID of a channel

1. Create a Telegram channel and add the Telegram bot to it.
1. Send a message to the channel. These two steps are important - if you
don't do them, you won't be getting meaningful information from the bot.
1. Open this URL from a web browser `https://api.telegram.org/bot{your_bot_token}/getUpdates`.
Make sure to replace `{your_bot_token}` with the actual token for your bot
that was obtained in the previous section.
1. You'll see a JSON response like

```json
    {
      "ok": true,
      "result": [
        {
          "update_id": 838xxxx36,
          "channel_post": {...},
            "chat": {
              "id": -1001xxxxxx062,
              "title": "....",
              "type": "channel"
            },
            "date": 1703065989,
            "text": "test"
          }
        }
      ]
    }
```

<!-- markdownlint-disable MD029 -->
5. Check the value of `result[0]["channel_post"]["chat"]["id"]` - it contains
the channel ID: `-1001xxxxxx062`
1. Try sending a test message to the channel. Use your browser to access the
URL `https://api.telegram.org/bot{your_bot_token}/sendMessage?chat_id={your_channel_ID}&text=test123`.
Make sure to replace `{your_bot_token}` with the bot token obtained in the
first section and `{your_channel_id}` with the channel ID obtained in the
previous point.
1. If the bot token and the chat ID are specified correctly, the message
`test123` will be displayed in the Telegram channel.
<!-- markdownlint-enable MD029 -->

## Configure the honeypot

- Stop the honeypot, if it is running.
- Open the file `etc/honeypot.cfg` and uncomment the `[output_telegram]`
section in it.
- Set the variables `bot_token` and `chat_id` to the bot token and the channel
ID that you have obtained in the previous sections.
- Optionally, set the variable `delay` to something different than the default
value of `2.0`. This is the delay, in seconds, between any two messages sent
to the bot. Note that if this number is smaller than `1.0`, Telegram's rate
limiting will cause it to return `429 Too many requests` errors.
- Set the variable `enabled` to `true`.
- Launch the honeypot.
